#include "WebServer.h"

#include <QTcpServer>

#include "ConnectionHandle.h"

using namespace WebServerML;

class WebServerML::WebServerImplementation : public QTcpServer
{
  void incomingConnection(qintptr socketDescriptor) override
  {
    ConnectionHandle *thread = new ConnectionHandle(socketDescriptor, this);
    connect(thread, &ConnectionHandle::finished, thread, &ConnectionHandle::deleteLater);
    thread->start();
  }
};

WebServer::WebServer(QObject* _parent) : QObject(_parent)
{
}

WebServer::~WebServer()
{
  stop();
}

int WebServer::port() const
{
  return m_port;
}

void WebServer::setPort(int _port)
{
  m_port = _port;
  emit(portChanged());
}

bool WebServer::start()
{
  stop();
  
  m_webServerImplementation = new WebServerImplementation;
  return m_webServerImplementation->listen(QHostAddress::Any, m_port);
}

void WebServer::stop()
{
  if(m_webServerImplementation)
  {
    m_webServerImplementation->close();
    m_webServerImplementation->deleteLater();
    m_webServerImplementation = nullptr;
  }
}

#include "moc_WebServer.cpp"
