#pragma once

#include <QQmlExtensionPlugin>

class WebServerMLPlugin : public QQmlExtensionPlugin {
  Q_OBJECT
  Q_PLUGIN_METADATA(IID "WebServerMLPlugin/1.0")
public:
  
  void registerTypes(const char* uri) override;
};
