#include "ConnectionHandle.h"

#include <QtNetwork>

ConnectionHandle::ConnectionHandle(int socketDescriptor, QObject *parent)
    : QThread(parent), socketDescriptor(socketDescriptor)
{
}

void ConnectionHandle::run()
{
    QTcpSocket tcpSocket;

    if (!tcpSocket.setSocketDescriptor(socketDescriptor)) {
        emit error(tcpSocket.error());
        return;
    }

    qDebug() << tcpSocket.readAll();
}
