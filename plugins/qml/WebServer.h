#pragma once

#include <QObject>

namespace WebServerML
{
  class WebServerImplementation;
  class WebServer : public QObject
  {
    Q_OBJECT
    Q_PROPERTY(int port READ port WRITE setPort NOTIFY portChanged)
  public:
    WebServer(QObject* _parent = nullptr);
    ~WebServer();
    int port() const;
    void setPort(int _port);
    Q_INVOKABLE bool start();
    Q_INVOKABLE void stop();
  signals:
    void portChanged();
  private:
    int m_port = 8080;
    WebServerImplementation* m_webServerImplementation = nullptr;
  };
};
