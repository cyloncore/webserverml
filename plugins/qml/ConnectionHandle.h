
#include <QThread>
#include <QTcpSocket>

class ConnectionHandle : public QThread
{
    Q_OBJECT

public:
    ConnectionHandle(int socketDescriptor, QObject *parent);

    void run() override;

signals:
    void error(QTcpSocket::SocketError socketError);

private:
    int socketDescriptor;
};
