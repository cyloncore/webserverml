#include "WebServerMLPlugin.h"

#include <QtQml>

#include "WebServer.h"

using namespace WebServerML;

void WebServerMLPlugin::registerTypes(const char* /*uri*/)
{
  const char* uri_WebServerML = "WebServerML";
  qmlRegisterType<WebServer>(uri_WebServerML, 1, 0, "WebServer");

}

#include "moc_WebServerMLPlugin.cpp"
